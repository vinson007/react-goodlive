# react-goodlive

#### 介绍
宜居项目，react入门级项目

#### 技术栈
React + ReactHook + ReactRouter + Redux + Axios + Less + 第三方

#### 第三方
1. 焦点轮播图
    - 参考文档：https://react-swipeable-views.com/
    - 安装依赖：
        ```shell
        npm install --save react-swipeable-views
        npm install --save react-swipeable-views-utils
        ```


#### 搭建服务器环境提供数据
- 运行服务器，提供模拟数据
    ```shell
        cd server
        node index.js
    ```

#### 安装教程
- 运行react项目
    ```shell
        git clone https://gitee.com/vinson007/react-goodlive.git
        npm i
        npm start
    ```

#### 打包项目

```shell
    npm run build
```
