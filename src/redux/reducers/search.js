import {UPDATE_SEARCH} from '../constants'

export default function search(state = '', action) {
    switch (action.type) {
        case UPDATE_SEARCH:
            return action.search
        default:
            return state
    }
}