import React from 'react'
import {HashRouter as Router, Route,Switch} from 'react-router-dom'
import Layout from '../pages/Main/Layout'
import Home from '../pages/Main/Home'
import LifeService from '../pages/Main/LifeService'
import Shop from '../pages/Main/Shop'
import User from '../pages/Main/User'
import BottomNav from '../components/BottomNav'
import City from '../pages/City'
import Search from '../pages/Search'
import Detail from '../pages/Detail'

export default function AppRouter() {
    return (
        <Router>
            <Switch>
                <Route path='/city' component={City} />
                <Route path='/search/:keyword' component={Search} />
                <Route path='/detail/:id' component={Detail} />
                <Layout to='/'>
                    <BottomNav/>
                    <Switch>
                        <Route exact path='/' component={Home} />
                        <Route path='/life' component={LifeService} />
                        <Route path='/shop' component={Shop} />
                        <Route path='/user' component={User} />
                    </Switch>
                </Layout>
            </Switch>
        </Router>
    )
}
