import React from 'react'
import PubHeader from '../../components/PubHeader'
import CurrentCity from './CurrentCity'
import CityList from './CityList'
import {useSelector, useDispatch} from 'react-redux'
import {changeCity} from '../../redux/actions/city'
import {withRouter} from 'react-router-dom'

function City(props) {

    // const [city,setCity] = useState('')
    const dispatch = useDispatch()
    const city = useSelector(state => state.city)

    function getCity(cityName) {
        // setCity(cityName)
        dispatch(changeCity(cityName))
        props.history.go(-1)
    }

    return (
        <>
            <PubHeader title='城市选择' />
            <CurrentCity city={city.cityName} />
            <CityList onEvent={getCity} />
        </>
    )
}

export default withRouter(City)
