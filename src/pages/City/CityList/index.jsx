import React from 'react'
import './style.less'

export default function CityList(props) {
    const cityList = ['北京','上海','深圳','广州','沈阳','天津','杭州','西安','青岛','南京','郑州','成都','赤峰','呼和浩特','次渠']

    function getCityName(e) {
        const {innerText} = e.target
        props.onEvent(innerText)
    }

    return (
        <div className="city-list-container">
            <h3>热门城市</h3>
            <ul className="clear-fix">
                {
                    cityList.map((item,index) => {
                        return(
                            <li key={index} onClick={e => getCityName(e)}>
                                <span>{item}</span>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}
