import React,{useEffect, useState} from 'react'
import DetailView from '../DetailView'
import api from '../../../api'

export default function DetailList(props) {

    const [detailData, setDetailData] = useState(null)

    useEffect(() => {
        api.detail(props.id).then(res => {
            const {code, result} = res.data
            if(code === 200) {
                setDetailData(result)
            }
        })
    }, [props.id])

  return (
      <>
          {
              detailData ? <DetailView data={detailData} /> : <div>等待数据加载....</div>
          }
      </>
  )
}
