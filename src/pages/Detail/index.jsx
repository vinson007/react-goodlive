import React from "react";
import {useParams} from 'react-router-dom'
import PubHeader from '../../components/PubHeader'
import DetailList from "./DetailList";

export default function Detail() {
    const params = useParams()
    return (
        <>
            <PubHeader title="详情页" />
            <DetailList id={params.id} />
        </>
    )
}