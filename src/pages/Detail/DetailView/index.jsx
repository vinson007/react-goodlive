import React from 'react'
import './style.less'
import Swiper from '../../../components/Swiper'

export default function DetailView(props) {

    const {data} = props

  return (
    <div>
        <Swiper banners={data.imgs} />
        <div className="detail-info">
            <h3>{data.title}</h3>
            <div className="box">
                <ul>
                    <li>
                        <span>{data.price} /月</span>
                        <p>租金</p>
                    </li>
                    <li>
                        <span>{data.info.type}</span>
                        <p>房屋类型</p>
                    </li>
                    <li>
                        <span>{data.houseType}</span>
                        <p>面积</p>
                    </li>
                </ul>
            </div>
            <div className="info">
                <div className="info-list">
                    <p>类型：{data.info.type}</p>
                    <p>朝向：{data.info.orientation}</p>
                </div>
                <div className="info-list">
                    <p>楼层：{data.info.level}</p>
                    <p>装修：{data.info.style}</p>
                </div>
                <div className="info-list">
                    <p>年代：{data.info.years}</p>
                </div>
            </div>
        </div>
    </div>
  )
}
