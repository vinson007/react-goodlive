import React,{useState} from "react";
import {Link} from 'react-router-dom'
import "./style.less";
import DefaultImg from '../../../../assets/images/default.png'
import loadImageAsync from "../../../../utils/loadImageAsync";

export default function Item(props) {
  const { data } = props;
  const [imgUrl, setImgUrl] = useState(DefaultImg)
  loadImageAsync(data.img).then(res => {
    setImgUrl(res)
  })
  return (
    <div className="list-item">
      <Link to={`/detail/${data.id}`}>
        <img src={imgUrl} alt="" />
        <div className="mask">
          <div className="left">
            <p>{data.title}</p>
            <p>{data.houseType}</p>
          </div>
          <div className="right">
            <div className="btn">{data.rentType}</div>
            <p dangerouslySetInnerHTML={{ __html: data.price + "元/月" }}></p>
          </div>
        </div>
      </Link>
    </div>
  );
}
