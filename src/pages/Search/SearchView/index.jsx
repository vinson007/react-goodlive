import React from 'react'
import Item from './Item'

export default function SearchView(props) {
    return (
        <div>
            {
                props.searchData.map(item => {
                    return <Item data={item} key={item.id} />
                })
            }
        </div>
    )
}
