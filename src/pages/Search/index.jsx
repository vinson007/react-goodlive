import React from 'react'
import { useParams } from 'react-router'
import SearchList from './SearchList'
import SearchHeader from './SearchHeader'

export default function Search() {

    const search= useParams()

    return (
        <>
            <SearchHeader />
            <SearchList search={search.keyword} />
        </>
    )
}
