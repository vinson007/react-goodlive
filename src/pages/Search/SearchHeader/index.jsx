import React from 'react'
import './style.less'
import SearchInput from '../../../components/SearchInput'
import { withRouter } from 'react-router-dom'

function SearchHeader(props) {

    function backHandle() {
        props.history.goBack()
    }

    return (
        <div id="search-header" className="clear-fix">
            <span className="back-icon float-left" onClick={backHandle}>
                <i className="icon-chevron-left"></i>
            </span>
            <div className="input-container">
                <i className="icon-search"></i>
                <SearchInput />
            </div>
        </div>
    )
}

export default withRouter(SearchHeader)
