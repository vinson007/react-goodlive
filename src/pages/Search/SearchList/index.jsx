import React,{useState,useEffect} from 'react'
import api from '../../../api'
import SearchView from '../SearchView'
import LoadMore from '../../../components/LoadMore'
import '../../../components/LoadMore/style.less'
import {useDispatch} from 'react-redux'
import {updateSearch} from '../../../redux/actions/search'

export default function SearchList(props) {
    
    const [searchData, setSearchData] = useState([])
    const [hasMore,setHasMore] = useState(false)
    const {search} = props
    const dispatch = useDispatch()

    useEffect(() => {
        http()
        dispatch(updateSearch(search))
    }, [search]) // eslint-disable-line

    function loadMoreHandle() {
        http()
    }

    function http() {
        api.search(search).then(res => {
            const {code, result} = res.data
            if(code === 200) {
                setSearchData(data => {
                    return data.concat(result.data)
                })
                setHasMore(result.hasMore)
            }
        })
    }

    return (
        <>
            {
                searchData.length > 0 ? <SearchView searchData={searchData} /> : <div>等待数据加载</div>
            }
            {
                hasMore ? <LoadMore onLoadMore={loadMoreHandle} /> : <div className="loadMore">被看光了</div>
            }
        </>
    )
}
