import React,{useState,useEffect} from 'react'
import api from '../../../../api'
import HomeHotView from '../HomeHotView'

export default function HomeHotList() {

    const [hot1List,setHot1List] = useState([])
    const [hot2List,setHot2List] = useState([])

    useEffect(()=> {
        api.getHomeHot1().then(res => {
            const {code, result} = res.data
            if(code === 200) {
                setHot1List(result)
            }
        })
    },[])

    useEffect(()=> {
        api.getHomeHot2().then(res => {
            const {code, result} = res.data
            if(code === 200) {
                setHot2List(result)
            }
        })
    },[])

    return (
        <>
            {
                hot1List.length > 0 ? <HomeHotView data={hot1List} title='新品推荐' /> : <div>等待数据加载</div>
            }
            {
                hot2List.length > 0 ? <HomeHotView data={hot2List} title='热门推荐' /> : <div>等待数据加载</div>
            }
        </>
    )
}
