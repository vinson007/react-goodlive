import axios from "../utils/request";

/**
 * 路径地址
 */

const base = {
    baseUrl: 'http://localhost:5566',
    homehot1: '/api/home/hot1',
    homehot2: '/api/home/hot2',
    search: '/api/search',
    detail: '/api/detail'
}

/**
 * 请求方法
 */

const api = {
    // 获取热门数据
    getHomeHot1() {
        return axios.get(base.baseUrl + base.homehot1)
    },
    getHomeHot2() {
        return axios.get(base.baseUrl + base.homehot2)
    },
    // 搜索数据
    search(search) {
        return axios.get(base.baseUrl + base.search,{
            params: {search}
        })
    },
    // 详情页数据
    detail(id) {
        return axios.get(base.baseUrl + base.detail, {
            params: {id}
        })
    }
}

export default api;