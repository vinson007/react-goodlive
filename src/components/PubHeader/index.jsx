import React from 'react'
import {withRouter} from 'react-router-dom'
import './style.less'

function PubHeader(props) {

    function backHandle () {
        props.history.goBack()
    }
    return (
        <div id="common-header">
            <span className="back-icon" onClick={backHandle}>
                <i className="icon-chevron-left"></i>
            </span>
            <h1>{props.title}</h1>
        </div>
    )
}

export default withRouter(PubHeader)
