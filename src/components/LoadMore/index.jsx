import React,{useEffect,useRef} from 'react'
import './style.less'

export default function LoadMore(props) {

    const {onLoadMore} = props
    const more = useRef()
    // 防抖
    function debounce(fn,wait) {
        let timeId = null
        return () => {
            if(timeId !== null) clearTimeout(timeId)
            timeId = setTimeout(fn, wait);
        }
    }
     
    useEffect(() => {
        const viewportHeigth = document.documentElement.clientHeight
        const handle = () => {
            if(more.current) {
                const top = more.current.getBoundingClientRect().top
                if(viewportHeigth > top) {
                    onLoadMore()
                }
            }
        }
        window.addEventListener('scroll', debounce(handle, 200))
    }, []) // eslint-disable-line



    return (
        <div ref={more} className="loadMore">
            加载更多
        </div>
    )
}
