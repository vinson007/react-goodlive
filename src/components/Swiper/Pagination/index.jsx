import React from 'react'
import './style.less'

export default function Pagination(props) {
    const liArr = []
    const {len,currentIndex} = props
    liArr.length = len
    liArr.fill(1)
    return (
        <div className="swiper-pagination">
            <ul>
                {
                    liArr.map((li,index) => {
                        return (
                            <li key={index} className={index === currentIndex ? 'selected' : ''}></li>
                        )
                    })
                }
            </ul>
        </div>
    )
}
