export default function loadImageAsync(url) {
    return new Promise((resolve, reject) => {
        const image = new Image()
        image.src = url
        image.onload = () => {
            resolve(url)
        }
        image.onerror = () => {
            reject(new Error('Could not load image at ' + url))
        }
    })
}
