const express = require('express')
const router = express.Router()
const homehot = require('./data/home/homehot')
const searchData = require('./data/search')
const detailData = require('./data/detail')
const url = require('url')
const Mock = require('mockjs')
const Random = Mock.Random

// 首页热门数据
router.get("/home/hot1", (req, res) => {
    res.send({
        code: 200,
        result: homehot.hot1
    })
})

router.get("/home/hot2", (req, res) => {
    res.send({
        code: 200,
        result: homehot.hot2
    })
})

// 搜索数据
router.get("/search", (req, res) => {
    const search = url.parse(req.url, true).query.search
    console.log(search)
    const searchData = Mock.mock({
        hasMore: true,
        'data': [
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
        ]
    })
    res.send({
        code: 200,
        result: searchData
    })
})

// id: Math.random().toString().slice(2),
// title: "豪宅 · 使馆壹号院4居室-南",
// houseType: "17/19层| 4室1厅 - 273.97 ㎡",
// price: "<h3>130000</h3>",
// rentType: "整租",
// img: "http://iwenwiki.com/api/livable/search/1.jpg"
// 使用mock模拟数据
router.get("/mock", (req, res) => {
    let data = Mock.mock({
        'list': [
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600')
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
            {
                id: Random.id(),
                title: Random.ctitle(5, 10),
                houseType: '17/19层| 4室1厅 - 273.97 ㎡',
                price: '<h3>130000</h3>',
                rentType: "整租",
                img: Random.image('800x600', Random.color(), '#FFF', 'png', Random.ctitle())
            },
        ]
    })
    res.send({
        code: 200,
        result: data
    })
})

router.get('/detail', (req,res) => {
    const id = url.parse(req.url, true).query.id
    console.log(id)
    res.send({
        code: 200,
        result: detailData
    })
})

module.exports = router